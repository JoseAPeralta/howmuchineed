const express = require('express')
const passport = require('passport')
const jwtDecode = require('../utils/jwtDecode')
const IncomesServices = require('../services/incomes')
const cacheResponse = require('../utils/cacheResponse')
const { SIXTY_MINUTES_IN_SECONDS } = require('../utils/time')
require('../utils/auth/strategies/jwt')

function incomesAPI(app) {
  const router = express.Router({ mergeParams: true })
  app.use('/goals/:goalId/incomes', router)

  const incomeService = new IncomesServices()

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    async (req, res, next) => {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS)
      const { goalId } = req.params
      const { userId } = jwtDecode(req.headers.authorization)
      const income = req.body
      income.amount = Number(income.amount)
      try {
        const created = await incomeService.createIncome(goalId, income, userId)
        res.status(201).json(created)
      } catch (error) {
        next(error)
      }
    }
  )

  router.delete(
    '/:incomeId',
    passport.authenticate('jwt', { session: false }),
    async (req, res, next) => {
      const { goalId } = req.params
      const { incomeId } = req.params
      const { userId } = jwtDecode(req.headers.authorization)
      try {
        const deleted = await incomeService.deleteIncome(
          goalId,
          incomeId,
          userId
        )
        res.json(deleted)
      } catch (error) {
        next(error)
      }
    }
  )
}

module.exports = incomesAPI
