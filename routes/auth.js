const express = require('express')
const passport = require('passport')
const boom = require('@hapi/boom')
const jwt = require('jsonwebtoken')
const UsersServices = require('../services/users')
const validation = require('../utils/middlewares/validationHandler')
const usersSchema = require('../utils/schemas/users')
const { config } = require('../config/index')

require('../utils/auth/strategies/basic')

function authAPI(app) {
  const userService = new UsersServices()
  const router = express.Router()
  app.use('/auth', router)

  router.post(
    '/register',
    validation(usersSchema.createUser),
    async (req, res, next) => {
      const { name, password, email } = req.body
      try {
        const response = await userService.createUser({ name, password, email })
        res.status(201).json({ response })
      } catch (error) {
        next(error)
      }
    }
  )

  router.post('/token', async (req, res, next) => {
    // eslint-disable-next-line consistent-return
    passport.authenticate('basic', (error, user) => {
      try {
        if (error || !user) {
          next(boom.unauthorized())
        }

        req.login(user, { session: false }, async (err) => {
          if (err) {
            next(err)
          }
        })
        // eslint-disable-next-line no-underscore-dangle
        const payload = { email: user.email, userId: user._id }
        const token = jwt.sign(payload, config.authJwtSecret, {
          expiresIn: '150m',
        })

        return res.status(201).json({ access_token: token })
      } catch (err) {
        next(err)
      }
    })(req, res, next)
  })
}

module.exports = authAPI
