const express = require('express')
const passport = require('passport')
const GoalsServices = require('../services/goals')
const validation = require('../utils/middlewares/validationHandler')
const goalSchema = require('../utils/schemas/goals')
const jwtDecode = require('../utils/jwtDecode')
const cacheResponse = require('../utils/cacheResponse')
const { SIXTY_MINUTES_IN_SECONDS } = require('../utils/time')
const { upload } = require('../utils/middlewares/uploadFileHandler')

require('../utils/auth/strategies/jwt')

function goalsAPI(app) {
  const router = express.Router()
  app.use('/goals', router)

  const goalService = new GoalsServices()

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    async (req, res, next) => {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS)
      const { userId } = jwtDecode(req.headers.authorization)
      try {
        const goals = await goalService.getGoals(userId)
        res.json({ data: goals })
      } catch (error) {
        next(error)
      }
    }
  )

  router.get(
    '/:goalId',
    passport.authenticate('jwt', { session: false }),
    async (req, res, next) => {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS)
      const { goalId } = req.params
      const { userId } = jwtDecode(req.headers.authorization)
      try {
        const goal = await goalService.getGoal(goalId, userId)
        res.json({ data: goal })
      } catch (error) {
        next(error)
      }
    }
  )

  router.post(
    '/',
    passport.authenticate('jwt', { session: false }),
    upload.single('image'),
    validation(goalSchema.createGoal),
    async (req, res, next) => {
      const { userId } = jwtDecode(req.headers.authorization)
      const goal = req.body
      goal.totalAmount = Number(goal.totalAmount)
      goal.user = userId
      goal.incomes = []
      goal.currentAmount = 0
      if (req.file) {
        goal.image = req.file
      }
      try {
        const created = await goalService.createGoal(goal)
        res.status(201).json(created)
      } catch (error) {
        next(error)
      }
    }
  )

  router.patch(
    '/:goalId',
    passport.authenticate('jwt', { session: false }),
    upload.single('image'),
    validation(goalSchema.goalId, 'params'),
    validation(goalSchema.updateGoal),
    async (req, res, next) => {
      const { goalId } = req.params
      const { userId } = jwtDecode(req.headers.authorization)
      const goal = req.body
      if (goal.totalAmount) {
        goal.totalAmount = Number(goal.totalAmount)
      }
      if (req.file) {
        goal.image = req.file
        try {
          await goalService.deleteGoalImage(goalId, userId)
        } catch (error) {
          next(error)
        }
      }
      try {
        const updated = await goalService.updateGoal(goalId, goal, userId)
        res.json(updated)
      } catch (error) {
        next(error)
      }
    }
  )

  router.delete(
    '/:goalId',
    passport.authenticate('jwt', { session: false }),
    async (req, res, next) => {
      const { goalId } = req.params
      const { userId } = jwtDecode(req.headers.authorization)
      try {
        await goalService.deleteGoalImage(goalId, userId)
        const deleted = await goalService.deleteGoal(goalId, userId)
        res.json(deleted)
      } catch (error) {
        next(error)
      }
    }
  )
}

module.exports = goalsAPI
