const { MongoClient, ObjectId } = require('mongodb')
const { config } = require('../config/index')

// se utiliza el metodo encodeURIComponent
// por si alguno de los elementos contiene caracteres especiales (/&?"")
// este metodo permite codificarlos para evitar problemas
const DB_USER = encodeURIComponent(config.dbUser)
const DB_PASSWORD = encodeURIComponent(config.dbPassword)
const DB_HOST = encodeURIComponent(config.dbHost)
const DB_NAME = config.dbName
const MONGO_URI = `mongodb+srv://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`

class MongoLib {
  constructor() {
    this.client = new MongoClient(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    this.dbName = DB_NAME
  }

  connection() {
    return new Promise((resolve, reject) => {
      this.client.connect((error) => {
        if (error) {
          reject(error)
        }
        // eslint-disable-next-line no-console
        console.log('Conectado con exito a mongo')
        resolve(this.client.db(this.dbName))
      })
    })
  }

  getlAll(collection, query) {
    return this.connection().then(async (db) => {
      const result = await db.collection(collection).find(query).toArray()
      return result
    })
  }

  getById(collection, id, user) {
    return this.connection().then(async (db) => {
      const result = await db
        .collection(collection)
        .findOne({ _id: ObjectId(id), user })
      return result
    })
  }

  getOne(collection, query) {
    return this.connection().then(async (db) => {
      const result = await db.collection(collection).findOne(query)
      return result
    })
  }

  create(collection, data) {
    return this.connection().then(async (db) => {
      try {
        const result = await db.collection(collection).insertOne(data)
        return result
      } catch (error) {
        return error
      }
    })
  }

  update(collection, id, update, user) {
    return this.connection().then(async (db) => {
      try {
        const result = await db.collection(collection).updateOne(
          {
            _id: ObjectId(id),
            user,
          },
          update
        )
        return result
      } catch (error) {
        return error
      }
    })
  }

  delete(collection, id, user) {
    return this.connection().then(async (db) => {
      try {
        const result = await db
          .collection(collection)
          .deleteOne({ _id: ObjectId(id), user })
        return result
      } catch (error) {
        return error
      }
    })
  }

  aggregate(collection, aggregate) {
    return this.connection().then(async (db) => {
      try {
        const result = await db
          .collection(collection)
          .aggregate(aggregate)
          .toArray()
        return result
      } catch (error) {
        return error
      }
    })
  }
}

module.exports = MongoLib
