const assert = require('assert')
const proxyquire = require('proxyquire')

const { response } = require('express')
const { goals, GoalsServiceMock } = require('../utils/mocks/goals')

const testServer = require('../utils/testServer')

describe('routes - api - goals', () => {
  const route = proxyquire('../routes/', {
    '../services/goal': GoalsServiceMock
  })

  const request = testServer(route)

  describe("GET /", () => {
    it("estatus 200", (done) => {
      request.get('/').expect(200, done)
    })

    it('should response with content type json', (done) => {
      request.get('/').expect('content-type', /json/, done)
    })

    it('shoul response with not error', (done) => {
      request.get('/').end((err, res) => {
        assert.strictEqual(err, null)
        done()
      })
    })
    /*
    it('should respond with the list of products', (done) => {
      request.get('/').end((err, res) => {
        assert.deepEqual(res.body, {
          data: goals,
          message:'goals list'
        })
        done()
      })
    })
    */
  })
})