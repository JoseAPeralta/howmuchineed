const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const helmet = require('helmet')
const goalsRouter = require('./routes/goals')
const incomesRouter = require('./routes/incomes')
const authApiRouter = require('./routes/auth')
const {
  logsSuccess,
  logsErros,
  skipSuccess,
  skipErrors,
} = require('./utils/middlewares/logger')

const app = express()
// helmet
app.use(helmet())
// view engine setup
app.set('views', path.join(__dirname, 'views/pages'))
app.set('view engine', 'pug')

// loggers setup
app.use(logger('dev'))
app.use(logger('combined', { skip: skipSuccess, stream: logsErros }))
app.use(logger('combined', { skip: skipErrors, stream: logsSuccess }))

// middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

app.use(express.static(path.join(__dirname, 'public')))

// routes setup
// app.use('/', indexRouter);
incomesRouter(app)
goalsRouter(app)
authApiRouter(app)
// app.use('/auth', authApiRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
