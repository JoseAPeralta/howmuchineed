const fs = require('fs')
const MongoLib = require('../lib/mongo')

class GoalsServices {
  constructor() {
    this.collection = 'goals'
    this.mongoDB = new MongoLib()
  }

  async getGoals(userId) {
    const query = { user: userId }

    const goals = await this.mongoDB.getlAll(this.collection, query)
    return goals
  }

  async getGoal(goalId, userId) {
    const goal = await this.mongoDB.getById(this.collection, goalId, userId)
    return goal
  }

  async createGoal(goal) {
    const response = await this.mongoDB.create(this.collection, goal)
    const created = !!response.result.n
    return created ? { created, data: goal } : { created }
  }

  async updateGoal(goalId, data, userId) {
    const update = { $set: data }
    const response = await this.mongoDB.update(
      this.collection,
      goalId,
      update,
      userId
    )
    const modified = !!response.result.nModified
    return { updated: modified, updateData: data }
  }

  async deleteGoal(goalId, userId) {
    const response = await this.mongoDB.delete(this.collection, goalId, userId)
    const deleted = !!response.result.n
    return { deleted }
  }

  async deleteGoalImage(goalId, userId) {
    const response = await this.getGoal(goalId, userId)
    if (response && response.image.path) {
      try {
        fs.unlinkSync(response.image.path)
      } catch (error) {
        throw new Error('No se pudo eliminar la imagen')
      }
    }
  }
}

module.exports = GoalsServices
