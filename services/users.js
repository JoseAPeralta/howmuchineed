const bcrypt = require('bcrypt')
const MongoLib = require('../lib/mongo')

class UsersServices {
  constructor() {
    this.collection = 'users'
    this.mongoDB = new MongoLib()
  }

  async getUser(email) {
    const query = { email }
    const user = await this.mongoDB.getOne(this.collection, query)
    return user
  }

  async createUser(user) {
    const { name, password, email } = user
    const existEmail = await this.getUser(email)
    if (!existEmail) {
      const hashedPassword = await bcrypt.hash(password, 10)
      const response = await this.mongoDB.create(this.collection, {
        name,
        email,
        password: hashedPassword,
      })
      const created = !!response.result.n
      return { created, email }
    }
    return { message: 'The mail already exists' }
  }
}

module.exports = UsersServices
