const { ObjectId } = require('mongodb')
const GoalsServices = require('./goals')

class incomesServices extends GoalsServices {
  async calculateCurrentAmount(goalId) {
    const aggregate = [
      {
        $match: {
          _id: ObjectId(goalId),
        },
      },
      {
        $unwind: '$incomes',
      },
      {
        $group: {
          _id: '$_id',
          currentAmount: {
            $sum: '$incomes.amount',
          },
        },
      },
      {
        $project: { currentAmount: { $trunc: ['$currentAmount', 2] } },
      },
    ]
    const currentAmounts = await this.mongoDB.aggregate(
      this.collection,
      aggregate
    )
    const { currentAmount } = currentAmounts[0]
    return currentAmount
  }

  async updateCurrentAmount(goalId, userId) {
    const currentAmount = await this.calculateCurrentAmount(goalId)

    const update = { $set: { currentAmount } }
    await this.mongoDB.update(this.collection, goalId, update, userId)
    return currentAmount
  }

  async createIncome(goalId, income, userId) {
    const newIncome = income
    // eslint-disable-next-line no-underscore-dangle
    newIncome._id = ObjectId()
    const update = { $push: { incomes: income } }

    const response = await this.mongoDB.update(
      this.collection,
      goalId,
      update,
      userId
    )
    const created = !!response.result.nModified

    if (created) {
      const currentAmount = await this.updateCurrentAmount(goalId, userId)
      return { created, data: newIncome, currentAmount }
    }

    return { created }
  }

  async deleteIncome(goalId, incomeId, userId) {
    const update = { $pull: { incomes: { _id: ObjectId(incomeId) } } }
    const response = await this.mongoDB.update(
      this.collection,
      goalId,
      update,
      userId
    )
    const deleted = !!response.result.nModified

    if (deleted) {
      const currentAmount = await this.updateCurrentAmount(goalId, userId)
      return { deleted, currentAmount }
    }

    return { deleted }
  }
}

module.exports = incomesServices
