function validate(data, schema) {
  const { error } = schema.validate(data)
  return error
}

function validationHandler(schema, check = 'body') {
  return (req, res, next) => {
    const error = validate(req[check], schema)
    return error ? res.json(error) : next()
  }
}

module.exports = validationHandler
