const fs = require('fs')
const path = require('path');

const logsSuccess = fs.createWriteStream(path.join(__dirname, '../../logs/accessSuccess.log'), { flags: 'a' })
const logsErros = fs.createWriteStream(path.join(__dirname, '../../logs/accessErrors.log'), { flags: 'a' })

const skipSuccess = (req, res) => res.statusCode < 400
const skipErrors = (req, res) => res.statusCode >= 400

module.exports = { logsSuccess, logsErros, skipSuccess, skipErrors }