const multer = require('multer')
const path = require('path')
const { v4: uuidv4 } = require('uuid')

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/upload/')
  },
  filename(req, file, cb) {
    const fileName = uuidv4()
    const extName = path.extname(file.originalname)
    cb(null, fileName + extName)
  },
})

const fileFilter = (req, file, cb) => {
  const filetypes = /jpeg|jpg|png|gif/
  const mimetype = filetypes.test(file.mimetype)
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase())

  if (mimetype && extname) {
    return cb(null, true)
  }

  return cb(
    new Error(
      `File upload only supports the following filetypes - ${filetypes}`
    )
  )
}

const limits = { fileSize: 1000000 }

const upload = multer({
  storage,
  fileFilter,
  limits,
})

module.exports = { upload }
