const jwtDecode = require('jwt-decode')

function getUser(req, res, next) {
  const { userId } = jwtDecode(req.headers.authorization)
  req.userId = userId
  next()
}

module.exports = getUser
