const passport = require('passport')
const { Strategy, ExtractJwt } = require('passport-jwt')
const boom = require('@hapi/boom')
const { config } = require('../../../config/index')
const UserService = require('../../../services/users')

const userService = new UserService()

passport.use(
  new Strategy(
    {
      secretOrKey: config.authJwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
    async (tokenPayload, done) => {
      try {
        const user = await userService.getUser(tokenPayload.email)
        if (!user) {
          return done(boom.unauthorized(), false)
        }

        return done(null, user)
      } catch (error) {
        return done(error)
      }
    }
  )
)
