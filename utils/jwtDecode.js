const jwtDecode = require('jwt-decode')

function JwtDecode(token) {
  const decoded = jwtDecode(token)
  return decoded
}
module.exports = JwtDecode
