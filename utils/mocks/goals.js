// id ioseph = 5f0bc6c5f1b80ee11f2ec4ed
const goals = [
  {
    _id: '1a',
    userId: 'Ioseph Peralta',
    name: 'Playstation 5',
    totalAmount: 525.0,
    currentAmount: 250,
    image: '/images/Play 5.jpeg',
    finalDate: new Date('December 25, 2020'),
    description: 'Ahorros para comprarme mi play 5',
    incomes: [
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de cuadro',
        mount: 10,
        description: 'Cuadro vendido al abuelo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de llavero',
        mount: 10,
        description: 'llavero vendido a blady',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
    ],
  },
  {
    _id: '2b',
    userId: 'Ioseph Peralta',
    name: 'Xbox',
    totalAmount: 560.0,
    currentAmount: 350,
    image: '/images/Xbox.jpg',
    finalDate: new Date('December 25, 2020'),
    description: 'Ahorros para comprarme mi Xbox',
    incomes: [
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de cuadro',
        mount: 10,
        description: 'Cuadro vendido al abuelo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de llavero',
        mount: 10,
        description: 'llavero vendido a blady',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
    ],
  },
  {
    _id: '3c',
    userId: 'Ioseph Peralta',
    name: 'Wii',
    totalAmount: 325.0,
    currentAmount: 150,
    image: '/images/Wii.jpg',
    finalDate: new Date('December 25, 2020'),
    description: 'Ahorros para comprarme mi Wii',
    incomes: [
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de cuadro',
        mount: 10,
        description: 'Cuadro vendido al abuelo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de llavero',
        mount: 10,
        description: 'llavero vendido a blady',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
    ],
  },
  {
    _id: '4d',
    userId: 'Ioseph Peralta',
    name: 'Tablet',
    totalAmount: 600.0,
    currentAmount: 0,
    image: '/images/Tablet.jpg',
    finalDate: new Date('December 25, 2020'),
    description: 'Ahorros para comprarme mi Tablet',
    incomes: [
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de cuadro',
        mount: 10,
        description: 'Cuadro vendido al abuelo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de llavero',
        mount: 10,
        description: 'llavero vendido a blady',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
    ],
  },
  {
    _id: '5e',
    userId: 'Ioseph Peralta',
    name: 'Viaje a Disney',
    totalAmount: 11525.0,
    currentAmount: 1050,
    image: '/images/Disney.jpg',
    finalDate: new Date('December 25, 2020'),
    description: 'Ahorros para comprarme mi Viaje a Disney',
    incomes: [
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de cuadro',
        mount: 10,
        description: 'Cuadro vendido al abuelo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Venta de llavero',
        mount: 10,
        description: 'llavero vendido a blady',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
      {
        date: new Date('December 25, 2020'),
        name: 'Diente',
        mount: 100,
        description: 'El hada se lo llevo',
      },
    ],
  }
];

function  filteredGoalsMock(name) {
  return goals.filter(goal => goal.name.includes(name))
}

class GoalsServiceMock {

  constructor() {
    this.goals = goals
    this.id = '6f'
  }

  async getGoals() {
    return Promise.resolve(this.goals)
  }

  async createGoal() {
    return Promise.resolve(this.id)
  }
}

module.exports = {goals, filteredGoalsMock, GoalsServiceMock};