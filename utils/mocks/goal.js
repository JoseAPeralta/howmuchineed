const goal = {
  user: 'Ioseph Peralta',
  name: 'Playstation 5',
  totalAmount: 525.0,
  currentAmount: 250,
  image: "/images/Play 5.jpeg",
  finalDate: new Date('December 25, 2020'),
  description: 'Ahorros para comprarme mi play 5',
  incomes: [
    {
      date: new Date('December 25, 2020'),
      name: 'Venta de cuadro',
      mount: 10,
      description: 'Cuadro vendido al abuelo',
    },
    {
      date: new Date('December 25, 2020'),
      name: 'Venta de llavero',
      mount: 10,
      description: 'llavero vendido a blady',
    },
    {
      date: new Date('December 25, 2020'),
      name: 'Diente',
      mount: 100,
      description: 'El hada se lo llevo',
    },
    {
      date: new Date('December 25, 2020'),
      name: 'Diente',
      mount: 100,
      description: 'El hada se lo llevo',
    },
  ],
};

module.exports = goal;