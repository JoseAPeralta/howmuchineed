const Joi = require('@hapi/joi')

const userIdSchema = Joi.string().pattern(new RegExp('^[0-9a-fA-F]{24}$'))

const createUser = Joi.object({
  name: Joi.string().max(20).required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
})
const getToken = Joi.object({
  Username: Joi.string().email().required(),
  password: Joi.string().required(),
})
module.exports = {
  userIdSchema,
  createUser,
  getToken,
}
