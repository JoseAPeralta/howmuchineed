const Joi = require('@hapi/joi')

const goalIdSchema = Joi.string().pattern(new RegExp('^[0-9a-fA-F]{24}$'))

const goalId = Joi.object({
  goalId: goalIdSchema.required(),
})

const createGoal = Joi.object({
  // string, trim, min 2 characters, required
  name: Joi.string().min(3).max(60).trim().required(),
  // number, float, min 1.0, max 1000000.0, required
  totalAmount: Joi.number().min(1).max(1000000).required(),
  // number, float, min 1.0, max 1000000.0, required
  // currentAmount: Joi.number().min(0).max(0),
  // string, img
  image: Joi.string(),
  // date, no current o past date
  finalDate: Joi.date().min('now').required(),
  // string, max 1000000
  description: Joi.string().min(1).max(1000000),
  // array, no elements
  // incomes: Joi.array().max(0).required(),
})

const updateGoal = Joi.object({
  // string, trim, min 2 characters, required
  name: Joi.string().min(3).max(60).trim(),
  // number, float, min 1.0, max 1000000.0, required
  totalAmount: Joi.number().min(1).max(1000000),
  // number, float, min 1.0, max 1000000.0, required
  currentAmount: Joi.number().min(0).max(1000000),
  // string, img
  image: Joi.string(),
  // date, no current o past date
  finalDate: Joi.date().min('now'),
  // string, max 1000000
  description: Joi.string().min(1).max(1000000),
  // array, no elements
  incomes: Joi.array().max(0),
})

const deleteGoal = Joi.object({
  goalId: goalIdSchema.required(),
})

module.exports = {
  goalIdSchema,
  goalId,
  createGoal,
  updateGoal,
  deleteGoal,
}
